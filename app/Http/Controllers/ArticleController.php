<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent;
use App\Models\Article as Article;
use App\Models\State as States;
use App\Models\Category as Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use BD;
use storage;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $stateid = States::all();
        $categoryid = Category::all();
        $im = Article::all();
        return \View::make('article',compact('stateid','categoryid', 'im'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
       //dd($request->all());
        $data = Article::InsertArticle($request->all());
        $im = Article::all();
        return Redirect::to('article');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $im = Article::find($request->id);
        $im->title=$request->title;
        $im->author=$request->author;
        $im->conten=$request->conten;
        $im->path =$request->path ;
        $im->state_id =$request->state_id ;
        $im->category_id =$request->category_id ;
        $im->save();
        return Redirect::to('article');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::destroy($id);
    }
}