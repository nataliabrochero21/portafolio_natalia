<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class Article extends Model
{
    protected $table = 'articles';
    protected $fillable = ['title','author','conten', 'state_id','category_id', 'path'];
    protected $guarded = ['id' ];


  



        public static function InsertArticle($datos){
      Article::create($datos);

}

public function setPathAttribute($path){

    $name = Carbon::now()->second.$path->getClientOriginalName();
    $this->attributes['path'] = $name;
   //  dd($name);
    \Storage::disk('local')->put($name, \File::get($path));
  }



    public function State(){

    	return $this-> belongsTo('App\Models\State');
    }

     public function category(){

    	return $this-> belongsTo('App\Models\Category');
    }
    




      public function Coments(){

    	return $this-> hasMany ('App\Models\Coment');
    }



      public function Articles(){

    	return $this-> hasMany ('App\Models\Article');
    }


}
