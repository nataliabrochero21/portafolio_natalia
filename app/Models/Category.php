<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
     protected $table = 'categories';
    protected $fillable = ['name'];
    protected $guarded = ['id' ];
    protected $primaryKey = 'id';

 
     public static function InsertCategory($datos){
    	Category::create($datos);

}

 public static function updateCategory($data){
            Clientes::where('id', $data['id'] )->update(['name' => $data['name']]);

            } 

     public function Articles(){

    	return $this->hasMany('App\Models\Article');
    }


     public function Potfolios(){

    	return $this->hasMany('App\Models\Potfolio');
    }

   
}
