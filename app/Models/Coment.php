<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coment extends Model
{
    protected $table = 'coments';
    protected $fillable = [ 'id', 'conten', 'FKarticles','FKuser_id'];
    protected $guarded = ['id' ];

 
    public function Article(){

    	return $this-> belongsTo('App\Models\Article')
    }

      public function User(){

    	return $this-> belongsTo('App\Models\User')
    }

}
