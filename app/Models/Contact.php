<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
     protected $table = 'contacts';
    protected $fillable = ['name','email','message'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';


    public static function InsertContact($datos){
    	Contact::create($datos);

    }
}
