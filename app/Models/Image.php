<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
     protected $table = 'images';
    protected $fillable = [ 'id', 'name', 'FKimages_id'];
    protected $guarded = ['id' ];


     public function Article(){

    	return $this-> belongsTo('App\Models\Article')
    }
}
