<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;




class Potfolio extends Model
{
    protected $table = 'potfolios';
    protected $fillable = [ 'id', 'title','descrption','state_id', 'image','category_id'];
    protected $guarded = ['id' ];

    protected $primaryKey = 'id';


    public static function InsertPortafolio($datos){
    Potfolio::create($datos);


    }


    public function State(){

    	return $this-> belongsTo('App\Models\State');
    }

     public function Cateory(){

    	return $this-> belongsTo('App\Models\Cateory');
    }
    


}
