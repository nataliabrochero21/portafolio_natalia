<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{

     protected $table = 'states';
    protected $fillable = [ 'id', 'name'];
    protected $guarded = ['id' ];
    protected $primaryKey =  'id';

    public static function InsertState($datos){

    	State::create($datos);
    }

 
      public function Articles(){

        return $this->hasMany('App\Models\Coment');
    }


        public function Potfolios(){

        return $this->hasMany('App\Models\Potfolio');
    }
}
