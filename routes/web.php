<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('/', 'welcome');

//articulo
Route::resource('article', 'articleController');
Route::post('article/store', ['as'=>'article/store', 'uses'=>'articleController@store']);
Route::post('article/update', ['as'=>'article/update', 'uses'=>'articleController@update']);
Route::post('article/destroy', ['as'=>'article/destroy', 'uses'=>'articleController@destroy']);

//contactame

Route::resource('welcome', 'contactController');
Route::post('contact/store', ['as'=>'contact.store', 'uses'=>'contactController@store']);





Route::get('/article', ['as'=>'/article', 'uses'=>'ArticleController@index']);
